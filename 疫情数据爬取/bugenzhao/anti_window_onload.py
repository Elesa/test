from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

options = webdriver.ChromeOptions()
options.headless = True  # Do not show UI
driver = webdriver.Chrome(options=options)
url = "http://wsjkw.sh.gov.cn/xwfb/index.html"
driver.get(url)

try:
    element = WebDriverWait(driver, 10).until(  # Wait up to 10 seconds
        EC.presence_of_element_located((By.ID, "main"))  # Modify "main" according to the actual page
    )
finally:
    print(driver.page_source)
    elements = driver.find_elements_by_xpath('//*[@id="main"]/div[2]/div/ul/li')
    for element in elements:
        print(element.find_element_by_xpath('a').text)
    driver.quit()
