# -*- coding: utf-8 -*-


        
import json
import requests
import csv

# 此处需要ak(与ip绑定)，ak申请地址：http://lbsyun.baidu.com/apiconsole/key?application=key
ak = "D4lufWbgiOIGKeAGIGeQGemjnsYnRQ5F"
u = 'http://api.map.baidu.com/geocoding/v3/?'
output = 'json'
# 需要获取的城市json文件,此处采用2.9日文件
load_filename = '20200209_migration_wuhan_1581332423.json'
# 输出文件
dump_filename = 'province_lng_lat.csv'

def amp_geocode(addr=None):
    url = u+ 'address=' + addr  + '&output=' + output + '&ak=' + ak
    res = requests.get(url)
    temp=json.loads(res.text)
    csv_writer.writerow([addr,temp['result']['location']['lng'],temp['result']['location']['lat']])

if __name__ == "__main__":
    load_f = open(load_filename,'r',encoding='utf8')
    addrs = json.load(load_f)
    load_f.close()
    with open(dump_filename,'w',newline='',encoding='utf8') as f:
        csv_writer = csv.writer(f)
        for addr in addrs:
            try:
                amp_geocode(addr)
            except:
                #存在部分省会百度接口经纬度缺失，例如西藏自治区，海南省。添加省会的地址。
                print(addr+'未找到地理位置')
        f.close()

