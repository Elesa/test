# nCov-2019-Analysis
## 武汉迁徙

```
.
├── 2020....
│   ├── migration_wuhan_xx.json         # 武汉每日迁出至各省的详细数量（规模指数 * 比例 * 100）！
│   └── migration_wuhan_city_xx.json    # 武汉每日迁出至各市的详细数量（规模指数 * 比例 * 100）！
├── README.md
├── migration_wuhan.ipynb       
├── migration_wuhan_city.ipynb
├── qianxi.ipynb
├── qianxi_bz.ipynb             # 迁徙比例
├── scale_index.csv             # 武汉每日迁出规模指数
└── scale_index.ipynb           # 每日迁徙规模指数爬虫
```

*~~凡2月8日后获取的「2月6日的市级迁出数据」可能存在异常，请参考 `./20200206/` 下的 `migration_wuhan_city_1581079140.json`。~~*

*现根据 `./20200206/migration_wuhan_city_1581079140.json` 作为市级迁出数据的基准数据，2月6日数据正常*

### 说明

1. 运行方法：
    - `./scale_index.ipynb` 生成最新每日「迁徙规模指数」到 `scale_index.csv`;
    - `./migration_wuhan_city.ipynb` 根据 `csv` 文件中的日期获得对应 *每日*「武汉迁出至各市的详细数量」，保存到 `2020xxxx/migration_wuhan_city_xx.json` 下;
    - `./migration_wuhan.ipynb` 是省级数据
2. 迁徙规模指数是百度定义概念，可当作线性处理，不同城市间可以横向比较
3. 迁徙数量定义为：当日迁出规模指数 * 迁出到该城市的比例 * 100
4. 百度2月6日的城市级迁出数据有误，现 2月6日 前的数据均依赖于 `20200206` 目录下的历史数据